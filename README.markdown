# Package manager

### Goals
* no automatic dependency resolution: you know best what your dependencies are
* better git submodules
  * track based on branch
    * use git describe instead of simple commit sha, this way you know what is a newer change in case of conflict
    * example:
      `develop:v1.0-ff34ead-10` newer than `develop:v1.0-ae2fd2a-6`
  * track based on tag
    * example:
      `tag:v1.0`, `tag:v2.1`
* manage multiple packages
* each package can be in two forms: source, binary
* easily switch between source and binary form


### Technical details

Packages configuration file:
```
{
  "packages" : [
    {
      "name" : "core",
      "source" : {
        "scm" : "git",
        "address" : "../core.git"
      },
      "binary" : {
        "protocol" : "sftp",
        "address" : "sftp.mobiledev.com",
        "path" : "/data/lib/core"
      },
      "version" : "develop:v1.0-ff34ead-10",
      # relative to config file location
      "path" : "lib/core"
    }
  ]
}
```

Binary repository directory structure:
```
/data/lib/core:
v1.0
v2.1
v1.0-ff34ead-10
v1.0-ae2fd2a-6
```

Binary package structure:
```
v1.0:
package.zip

v1.0-ae2fd2a-6:
package.zip
```

Source package structure:
```<repository content>```

### Example command line usage

Update package to currently checked out version:
```
$ pkg update-package core
updated to develop:v2.1-ae2fd2a-6
```

Reset package to version from packages configuration file:
```
$ pkg reset-package core
reset to tag:v2.1
```