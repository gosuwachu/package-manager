import json
import os
import distribution
import package


class FileConfigLoader:
    def __init__(self, path):
        self.path = path

    def load(self):
        return open(self.path, "r").read()

    def save(self, content):
        open(self.path, "w").write(content)


class StringConfigLoader:
    def __init__(self, content):
        self.content = content

    def load(self):
        return self.content

    def save(self, content):
        self.content = content


class Config:
    def __init__(self, config_loader):
        self.cache_path = ".bbpkg_cache"
        self.package_cache_path = os.path.join(self.cache_path, "packages")
        self.config_loader = config_loader
        self.__parse_config_file()

    def __parse_config_file(self):
        config = json.loads(self.config_loader.load())
        self.__parse_packages(config.get("packages", []))

    def __parse_packages(self, packages_dict):
        self.packages = []
        for packageDict in packages_dict:
            self.__parse_package(packageDict)

    def __parse_package(self, package_dict):
        package_name = package_dict["name"]
        pkg = package.Package(
            name=package_name,
            version=package_dict["version"],
            path=package_dict["path"],
            distribution=package_dict["distribution"],
            source=self.__parse_distribution(package_name, package_dict.get("source", None)),
            binary=self.__parse_distribution(package_name, package_dict.get("binary", None)))
        self.packages.append(pkg)

    def __parse_distribution(self, package_name, distribution_dict):
        dist_type = distribution_dict.get("type", None)
        if dist_type == distribution.CP.TYPE:
            return self.__parse_cp(package_name, distribution_dict)
        elif dist_type == distribution.Git.TYPE:
            return self.__parse_git(package_name, distribution_dict)
        else:
            return None

    def __parse_cp(self, package_name, distribution_dict):
        return distribution.CP(
            self,
            package_name,
            distribution_dict["path"])

    def __parse_git(self, package_name, distribution_dict):
        return distribution.Git(
            self,
            package_name,
            distribution_dict["address"])

    def save(self):
        config = self.__serialize()
        content = json.dumps(config, indent=2)
        self.config_loader.save(content)

    def __serialize(self):
        config = dict()
        config["packages"] = self.__serialize_packages()
        return config

    def __serialize_packages(self):
        packages_list = list()
        for package_obj in self.packages:
            packages_list.append(self.__serialize_package(package_obj))
        return packages_list

    def __serialize_package(self, package_obj):
        package_dict = dict()
        package_dict["name"] = package_obj.name
        package_dict["version"] = package_obj.version
        package_dict["path"] = package_obj.path
        package_dict["distribution"] = package_obj.distribution
        if package_obj.source:
            package_dict["source"] = self.__serialize_distribution_info(package_obj.source)
        if package_obj.binary:
            package_dict["binary"] = self.__serialize_distribution_info(package_obj.binary)
        return package_dict

    def __serialize_distribution_info(self, dist):
        source_dict = dict()
        source_dict["type"] = dist.TYPE
        if dist.TYPE == distribution.CP.TYPE:
            self.__serialize_cp(source_dict, dist)
        elif dist.TYPE == distribution.Git.TYPE:
            self.__serialize_git(source_dict, dist)
        return source_dict

    @staticmethod
    def __serialize_cp(source_dict, dist):
        source_dict["path"] = dist.source_path

    @staticmethod
    def __serialize_git(source_dict, dist):
        source_dict["address"] = dist.address
