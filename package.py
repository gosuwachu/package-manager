import os
import shutil
from distribution import DistributionAlreadyInitializedError, distutils
import util


class PackagePathAlreadyExists(Exception):
    def __init__(self, package_name, package_path):
        self.package_name = package_name
        self.package_path = package_path


class Package:
    BINARY_DISTRIBUTION = "binary"
    SOURCE_DISTRIBUTION = "source"

    def __init__(
            self,
            name,
            version,
            path,
            distribution,
            source,
            binary):
        self.binary = binary
        self.source = source
        self.distribution = distribution
        self.path = path
        self.version = version
        self.name = name

    def init(self):
        distribution = self.__current_distribution()
        self.__init_package(distribution)
        self.__reset_package(distribution, self.version)
        self.__link(distribution)

    def update_package(self):
        distribution = self.__current_distribution()
        new_version = distribution.get_current_version()
        self.version = new_version

    def reset_package(self):
        distribution = self.__current_distribution()
        self.__reset_package(distribution, self.version)

    def switch_distribution(self, new_distribution):
        if self.distribution == new_distribution:
            print("Distribution already set to {0}: nothing to do".format(new_distribution))
        else:
            self.__switch_distribution(new_distribution)

    def __switch_distribution(self, new_distribution):
        self.distribution = new_distribution
        self.init()

    def __current_distribution(self):
        if self.distribution == self.BINARY_DISTRIBUTION:
            return self.binary
        elif self.distribution == self.SOURCE_DISTRIBUTION:
            return self.source

    def __init_package(self, distribution):
        try:
            distribution.init()
            print("Init '{0}' from {1}... done.".format(self.name, self.distribution))
        except DistributionAlreadyInitializedError:
            print("Init '{0}' from {1}... already initialized.".format(self.name, self.distribution))

    def __link(self, distribution):
        package_parent_path = os.path.relpath(os.path.join(self.path, os.pardir))
        distutils.dir_util.mkpath(package_parent_path)

        source_link_path = os.path.relpath(distribution.cache_path, package_parent_path)
        with util.chdir(package_parent_path):
            link_name = os.path.relpath(self.path, package_parent_path)

            if "symlink" in dir(os):
                if os.path.exists(link_name):
                    if os.path.islink(link_name):
                        os.unlink(link_name)
                    else:
                        raise PackagePathAlreadyExists(self.name, self.path)
                os.symlink(source_link_path, link_name)
            else:
                if os.path.exists(link_name):
                    shutil.rmtree(link_name)
                shutil.copytree(source_link_path, link_name)

    def __reset_package(self, distribution, version):
        distribution.reset_package(version)