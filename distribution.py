import distutils.dir_util
import os
import subprocess
import sys
import util


class DistributionAlreadyInitializedError(Exception):
    pass


class CP:
    TYPE = "cp"

    def __init__(self, config, package_name, source_path):
        self.package_name = package_name
        self.config = config
        self.source_path = source_path
        self.cache_path = self.__cache_path()

    def init(self):
        if os.path.exists(self.cache_path) and os.listdir(self.cache_path):
            raise DistributionAlreadyInitializedError()
        distutils.dir_util.copy_tree(self.source_path, self.cache_path)

    def get_current_version(self):
        return "<latest>"

    def reset_package(self, version):
        pass

    def __cache_path(self):
        return os.path.join(self.config.package_cache_path, self.package_name, "cp")


class Git:
    TYPE = "git"

    def __init__(self, config, package_name, address):
        self.package_name = package_name
        self.config = config
        self.address = address
        self.cache_path = self.__cache_path()

    def init(self):
        distutils.dir_util.mkpath(self.cache_path)
        if os.path.exists(os.path.join(self.cache_path, ".git")):
            self.__fetch()
            raise DistributionAlreadyInitializedError()

        subprocess.check_call(
            ["git", "clone", self.address, self.cache_path],
            stdout=sys.stdout,
            stderr=sys.stderr)

    def get_current_version(self):
        with util.chdir(self.cache_path):
            describe = subprocess.check_output(
                ["git", "describe", "--tags", "--always", "--dirty"],
                stderr=sys.stderr).strip()

            tags_at_head = subprocess.check_output(
                ["git", "tag", "--points-at", "HEAD"],
                stderr=sys.stderr).strip().split("\n")

            branch = subprocess.check_output(
                ["git", "rev-parse", "--abbrev-ref", "HEAD"],
                stderr=sys.stderr).strip()

            if branch != "HEAD" and describe not in tags_at_head:
                return branch + ":" + describe
            return describe

    def reset_package(self, version):
        revision = self.__revision_from_version(version)
        with util.chdir(self.cache_path):
            subprocess.check_call(
                ["git", "checkout", revision],
                stdout=sys.stdout,
                stderr=sys.stderr)

    def __revision_from_version(self, version):
        separator = version.find(":")
        return version[separator+1:]

    def __cache_path(self):
        return os.path.join(self.config.package_cache_path, self.package_name, "git")

    def __fetch(self):
        with util.chdir(self.cache_path):
            subprocess.check_call(
                ["git", "fetch", "--prune"],
                stdout=sys.stdout,
                stderr=sys.stderr)