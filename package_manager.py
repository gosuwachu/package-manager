import sys


class PackageNotFoundError(Exception):
    def __init__(self, package_name):
        self.package_name = package_name

    def __str__(self):
        return "'{0}': package not found".format(self.package_name)


class SystemLogger:
    def __init__(self):
        pass

    def fatal_error(self, message):
        sys.exit(message)


class PackageManager:
    def __init__(self, config):
        self.config = config

    def init(self):
        for package in self.config.packages:
            self.__init_package(package)

    def update_package(self, package_names):
        # treat empty package list as all packages
        if not package_names:
            package_names = [package.name for package in self.config.packages]
        for package in package_names:
            self.__update_package(package)
        self.config.save()

    def reset_package(self, package_names):
        # treat empty package list as all packages
        if not package_names:
            package_names = [package.name for package in self.config.packages]
        for package in package_names:
            self.__reset_package(package)

    def switch_distribution(self, package_name, new_distribution):
        package = self.__package_from_name(package_name)
        package.switch_distribution(new_distribution)
        self.config.save()

    def __init_package(self, package):
        package.init()

    def __update_package(self, package_name):
        package = self.__package_from_name(package_name)
        package.update_package()

    def __reset_package(self, package_name):
        package = self.__package_from_name(package_name)
        package.reset_package()

    def __package_from_name(self, package_name):
        package = next((package for package in self.config.packages if package.name == package_name), None)
        if not package:
            raise PackageNotFoundError(package_name)
        return package
