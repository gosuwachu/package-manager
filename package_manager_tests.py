import unittest
from config import StringConfigLoader, Config
from package_manager import PackageManager, PackageNotFoundError


CONFIG = """
{
  "packages" : [
    {
      "name" : "mxcore",
      "version" : "<latest>",
      "path" : "libmx/mxcore",
      "distribution" : "binary",
      "source" : {
        "scm" : "cp",
        "path" : "repositories/mxcore/source"
      },
      "binary" : {
        "protocol" : "cp",
        "path" : "repositories/mxcore/binary"
      }
    }
  ]
}
"""


class PackageManagerTestCase(unittest.TestCase):
    def setUp(self):
        self.config_loader = StringConfigLoader(CONFIG)
        self.config = Config(self.config_loader)
        self.manager = PackageManager(self.config)

    def test_update_package_return_error_when_package_no_found(self):
        with self.assertRaises(PackageNotFoundError):
            self.manager.update_package(["invalid name"])

    def test_reset_package_return_error_when_package_no_found(self):
        with self.assertRaises(PackageNotFoundError):
            self.manager.reset_package(["invalid name"])


if __name__ == '__main__':
    unittest.main()
