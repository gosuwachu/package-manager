#!/usr/bin/env python

"""BB Package Manager.

Usage:
  bbpkg.py init
  bbpkg.py update-package [<name>...]
  bbpkg.py reset-package [<name>...]
  bbpkg.py switch-distribution <package_name> (source|binary)
  bbpkg.py (-h | --help)
  bbpkg.py --version

Options:
  -h --help            Show this screen.
  --version            Show version.

Commands:
  init                 Download all packages.
  update-package       Update package to currently checked out version.
  reset-package        Reset package to version from packages configuration file.
  switch-distribution  Switch package to given distribution method.
"""
from docopt import docopt
from config import Config, FileConfigLoader
from package import Package
from package_manager import PackageManager, SystemLogger, PackageNotFoundError

if __name__ == '__main__':
    arguments = docopt(__doc__, version='BB Package Manager version 1.0')

    config_loader = FileConfigLoader(".bbpkg")
    config = Config(config_loader)

    logger = SystemLogger()
    manager = PackageManager(config)

    try:
        if arguments["init"]:
            manager.init()
        elif arguments["update-package"]:
            manager.update_package(arguments["<name>"])
        elif arguments["reset-package"]:
            manager.reset_package(arguments["<name>"])
        elif arguments["switch-distribution"]:
            distribution = None
            if arguments["source"]:
                distribution = Package.SOURCE_DISTRIBUTION
            elif arguments["binary"]:
                distribution = Package.BINARY_DISTRIBUTION
            manager.switch_distribution(arguments["<package_name>"], distribution)
    except PackageNotFoundError as ex:
        logger.fatal_error(ex)
